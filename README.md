# Unifi channels planner


## Channels optimization. Our approach.


![Channels example](channels.jpg)


### Assumptions.
- We have a lot of APs. Some are only 2g.
- Channels overlapping is a problem. 
- DFS channels automatic jump is a problem.
- Unifi controller hasn't an AI for these problems.

### Our approach.
- Reduce power trasmission on every AP so area overlapping is minimum.
- Put Unifi controller in debug mode, so generate a more verbose log.
- Using unifi controller api and server.log create two graph, one for 2g and one for 5g:    
    - every AP is a node
    - when there is an area overlapping from AP1 and AP2 then there is an edge from node AP1 and node AP2 in graph
- Compile a simple ini file contains preferred channels (e.g. 2g=1,6,11)    
- Using graph teoring  coloring graph in according channels overlapping constraints (and DFS constraints).
(Take a look at: https://en.wikipedia.org/wiki/Graph_coloring)
- Using open source software gephi see graphs and change channels in Unifi controller according. 


## Usage


In unifi controller switch to debug mode. Now every APs log into server.log neighbour.

In your PC (tested in osx) clone repository and execute: 

    ./analyze.sh 10.10.1.1 myPassword
    
Where:

- 10.10.1.1: unifi controller ip (or FQDN)
- myPassword: unifi controller password
    

This command generate two graphs:

 -  wireless_2g.dot
 -  wireless_5g.dot

If you want see graphs:

- Open Gephi (Download: https://gephi.org/)
- File > Open > "wireless_2g.dot" (or "wireless_5g.dot)
- Click "Overview"
    - In layout panel: Frucheterman Reingold > Run
- Click "Preview"
    - Check "Show labels"
    - Click "Refresh"

If you want to setup suggested channels:

    ./analyze-set.sh 10.10.1.1 myPassword


Each node is an AP.

If an AP see other AP than an edge is draw.


Before:
![Graph example](graph_1_before.png)

After:
![Graph example](graph_1_after.png)

Before:
![Graph example](graph_2_before.png)

After:
![Graph example](graph_2_after.png)




## TNG
Next generation of this script:

- a simple java software
- that runs on unifi controller host
- regular execution via crontab (every 24h)
- talks with mongodb server
- produce graphs
- colorized graphs
- set APs channels in according



#!/usr/bin/env bash


echo [1/2] Setting 2g channels
./application.php set --ip=$1 --password=$2 --filename=wireless_2g_optimized.dot
echo
echo [2/2] Setting 5g channels
./application.php set --ip=$1 --password=$2 --filename=wireless_5g_optimized.dot

#!/usr/bin/env bash

echo [1/4] Retrieve server.log file
scp root@$1:/var/log/unifi/server.log .


for r in `seq 40 0`; do

echo "[2/4] Create .dot files with rssi > $r"
./application.php create --ip=$1 --password=$2 --rssi=$r

#echo [3/4] Coloring 2g graph
#java -jar coloring.jar -i wireless_2g.dot  -c channels.txt -b 2g

echo [4/4] Coloring 5g graph
java -jar coloring.jar -i wireless_5g.dot  -c channels.txt -b 5g

done
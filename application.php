#!/usr/bin/env php
<?php
require __DIR__.'/vendor/autoload.php';
require __DIR__.'/vendor/malle-pietje/phpapi/class.unifi.php';


use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new CreateCommand());
$application->add(new SetCommand());
$application->add(new RebootCommand());
$application->add(new ListCommand());

$application->run();
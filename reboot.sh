#!/usr/bin/env bash

for m in `cat list.txt`; do
    echo "Reboot $m"
    ./application.php reboot --ip="$1" --password="$2" --mac=="$m"
done
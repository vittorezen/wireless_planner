<?php


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class CreateCommand extends Command
{
    private $graph_2g;
    private $graph_5g;

    protected function configure()
    {
        $this->setName('create')
            ->setDescription('Create graph')
            ->addOption(
                'ip',
                null,
                InputOption::VALUE_REQUIRED,
                'Unifi controller ip address',
                '127.0.0.1'
            )
            ->addOption(
                'username',
                null,
                InputOption::VALUE_REQUIRED,
                'username',
                'ubnt'
            )
            ->addOption(
                'password',
                null,
                InputOption::VALUE_REQUIRED,
                'Password',
                'ubnt'
            )
            ->addOption(
                'rssi',
                null,
                InputOption::VALUE_REQUIRED,
                'rssi',
                'ubnt'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->graph_2g = new Alom\Graphviz\Digraph('2G');
        $this->graph_5g = new Alom\Graphviz\Digraph('5G');

        $this->getNodes($input);
        $this->getEdges();
        $graph = $this->undirect($this->graph_5g->render());
        file_put_contents('wireless_5g.dot', $graph);
        $graph = $this->undirect($this->graph_2g->render());
        file_put_contents('wireless_2g.dot', $graph);
    }

    private function getNodes(InputInterface $input)
    {
        $ip = $input->getOption('ip');
        $username = $input->getOption('username');
        $password = $input->getOption('password');
        $this->rssiLimit = $input->getOption('rssi');
        $api = new unifiapi($username, $password, 'https://' . $ip . ':8443', 'default', '4');
        $api->login();
        foreach ($api->list_aps() as $ap) {
            $name = (@$ap->name) ? $ap->name : $ap->mac;

            if (@$ap->radio_ng) {
                $attributes = [];
                $attributes['label'] = $name;
                $attributes['mac'] = $ap->mac;
                if (@$ap->radio_ng->channel) {
                    $attributes['radio_ng_channel'] = $ap->radio_ng->channel;
                }
                if (@$ap->radio_ng->max_txpower) {
                    $attributes['radio_ng_max_txpower'] = $ap->radio_ng->max_txpower;
                }
                $this->graph_2g->node(md5($name), $attributes);
            }

            if (@$ap->radio_na) {
                $attributes = [];
                $attributes['label'] = $name;
                $attributes['mac'] = $ap->mac;
                if (@$ap->radio_na->channel) {
                    $attributes['radio_na_channel'] = $ap->radio_na->channel;
                }
                if (@$ap->radio_na->max_txpower) {
                    $attributes['radio_na_max_txpower'] = $ap->radio_na->max_txpower;
                }
                $this->graph_5g->node(md5($name), $attributes);
            }
        }
    }

    private function getEdges()
    {
        $rows = file('server.log');
        $mode = 'search-ap';
        $lastAPname = '';
        $min_rssi = 100000;
        $max_rssi = -1;
        foreach ($rows as $row) {
            if (($mode == 'search-ap') && (strpos($row, ' INFO  inform - from') === false)) {
                continue;
            }
            if ($mode == 'search-ap') {
                $APmac = substr($row, strpos($row, '[', 20) + 1, 17);
                $APname = substr($row, strpos($row, '(', 20) + 1);
                $APname = substr($APname, 0, strpos($APname, ','));
                if ($APname == '') {
                    $APname = $APmac;
                }
                $lastAPname = $APname;
                $mode = 'search-neigh';
                continue;
            }
            if (($mode == 'search-neigh') && (strpos($row, 'INFO  inform - from'))) {
                $mode = 'search-ap';
                continue;
            }

            if ((strpos($row, '[neigh]') === false)) {
                continue;
            }
            $row = substr($row, strpos($row, '[neigh]') + 8);
            $APname2 = substr($row, 0, strpos($row, '(') - 1);
            $rssi = substr($row, strpos($row, 'rssi') + 5);
            $rssi = substr($rssi, 0, strpos($rssi, ','));
            $min_rssi = min($min_rssi, $rssi);
            $max_rssi = max($max_rssi, $rssi);
            if (($rssi > $this->rssiLimit) && ($lastAPname != $APname2)) {
                $this->graph_2g = $this->addEdge(md5($lastAPname), md5($APname2), $this->graph_2g, $rssi);
                $this->graph_5g = $this->addEdge(md5($lastAPname), md5($APname2), $this->graph_5g, $rssi);
            }
        }

        // echo "\r\n RSSI [$min_rssi $max_rssi]";
    }

    private function addEdge($ap1, $ap2, Alom\Graphviz\Digraph $graph, $rssi)
    {
        try {
            $n1 = $graph->get($ap1);
            $n2 = $graph->get($ap2);
            if ((!$graph->hasEdge(array($ap1, $ap2))) && (!$graph->hasEdge(array($ap2, $ap1)))) {
                $graph->edge(array($ap1, $ap2), array('weight' => $rssi,'label' => $rssi));
            }
        } catch (InvalidArgumentException $e) {
            //echo "\r\n--------------------------------------------------------------------";
            //echo "\r\n $ap1";
            //echo "\r\n $ap2";
        }

        return $graph;
    }

    public function undirect($g)
    {
        $g = str_replace('digraph 2G', 'graph "2G"', $g);
        $g = str_replace('digraph 5G', 'graph "5G"', $g);
        $g = str_replace('->', '--', $g);
        $g = str_replace(';', '', $g);

        return $g;
    }
}

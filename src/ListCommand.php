<?php


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class ListCommand extends Command
{

    protected function configure()
    {
        $this->setName('list')
            ->setDescription('List antennas')
            ->addOption(
                'ip',
                null,
                InputOption::VALUE_REQUIRED,
                'Unifi controller ip address',
                '127.0.0.1'
            )
            ->addOption(
                'username',
                null,
                InputOption::VALUE_REQUIRED,
                'username',
                'ubnt'
            )
            ->addOption(
                'password',
                null,
                InputOption::VALUE_REQUIRED,
                'Password',
                'ubnt'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ip = $input->getOption('ip');
        $username = $input->getOption('username');
        $password = $input->getOption('password');
        $api = new unifiapi($username, $password, 'https://' . $ip . ':8443', 'default', '4');
        //$api->set_debug(true);
        $api->login();
        foreach ($api->list_aps() as $ap) {
            echo "\r\n".$ap->mac;
        }

    }




}

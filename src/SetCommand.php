<?php


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class SetCommand extends Command
{
    private $graph_2g;
    private $graph_5g;

    protected function configure()
    {
        $this->setName('set')
            ->setDescription('Set channels from graph')
            ->addOption(
                'ip',
                null,
                InputOption::VALUE_REQUIRED,
                'Unifi controller ip address',
                '127.0.0.1'
            )
            ->addOption(
                'username',
                null,
                InputOption::VALUE_REQUIRED,
                'username',
                'ubnt'
            )
            ->addOption(
                'password',
                null,
                InputOption::VALUE_REQUIRED,
                'Password',
                'ubnt'
            )
            ->addOption(
                'filename',
                null,
                InputOption::VALUE_REQUIRED,
                'filename',
                'ubnt'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ip = $input->getOption('ip');
        $username = $input->getOption('username');
        $password = $input->getOption('password');
        $api = new unifiapi($username, $password, 'https://' . $ip . ':8443', 'default', '4');
        //$api->set_debug(true);
        $api->login();

        $filename = $input->getOption('filename');
        $raw = file_get_contents($filename);
        preg_match_all($this->getPattern(), $raw, $out);
        foreach ($out[0] as $node) {
            $attr = [];
            $elements = explode(" ", $node);
            $radio = "";
            foreach ($elements as $line) {
                $o = explode('=', $line);
                if (sizeof($o) == 2) {
                    $attr[$o[0]] = trim($o[1], '",');
                    if (substr($o[0], 0, 6) == 'radio_') {
                        $radio = substr($o[0], 6, 2);
                    }
                }
            }

            $aps = $api->list_aps($attr['mac']);
            $ap = $aps[0];
            if (@$ap->{$radio . '-channel'} != $attr['suggested_channel']) {
                $radio_table = $ap->{'radio_' . $radio};
                $api->set_ap_radiosettings($ap->_id, $radio, $attr['suggested_channel'], $radio_table->ht, $radio_table->tx_power_mode, $radio_table->tx_power);
                echo "\r\n set " . @$ap->name . ": " . @$ap->{$radio . '-channel'} . " -> " . $attr['suggested_channel'];
                //$api->restart_ap($attr['mac']);
            } else {
                echo "\r\n skip " . @$ap->name . ": " . $ap->{$radio . '-channel'} ;
            }
        }

    }


    private function getPattern()
    {
        return "/\[.*\]\;/";
    }


}
